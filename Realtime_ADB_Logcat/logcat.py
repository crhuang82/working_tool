#!/usr/bin/env python

import sys, time, datetime
from os import path, makedirs
import subprocess
from threading import Thread, Event
import re

# You could modify below array to decide which log you want
#LOG_LIST = ['main', 'radio']
LOG_LIST = ['main', 'radio', 'kernel']

LogcatCMD = {"main" : "logcat -b main -b system -v threadtime",
             "radio": "logcat -b radio -v threadtime",
             "kernel" : "shell dmesg",
             "kernel_r" : "shell su root dmesg",
             "get-state" : "get-state"}
             
CHECK_ADB_GAP_TIME = 0.1
KERNEL_LOG_CHECK_GAP = 1
KERNEL_LOG_SPLIT = False

if sys.platform.startswith('win'):
    PATH_BASE = "D:\Manuallog"
else:
    PATH_BASE = "/media/WD/Work/Workspace/Manuallog"


class AdbCommnd(Thread):
    def __init__(self, device, logPath, logtype):
        Thread.__init__(self)
        self.logtype = logtype
        self.stopLog = Event()
        self.logWriteDone = Event()
        self.device = device
        self.cmd = ' '.join(['adb', '-s', self.device, LogcatCMD[logtype]])
        if self.logtype.startswith('kernel'):
            if not subprocess.call(['adb', '-s', self.device, 'shell', 'su', '0', 'id', '-u '],stdout=subprocess.PIPE, stderr=subprocess.STDOUT):
                print '[Debug] shell is not rooted'
                self.cmd = ' '.join(['adb', '-s', self.device, LogcatCMD['kernel_r']])
            else:
                print '[Debug] shell is rooted'

        self.logPath = logPath
        self.logNum = 0

    def run(self):
        if self.logtype.startswith('kernel') and not KERNEL_LOG_SPLIT:
	    old_time = 0.0
        while not self.stopLog.isSet():
            self.logWriteDone.clear()
            logName = path.join(self.logPath, self.logtype + '_' + str(self.logNum) + '.log')
            log = open(logName, 'a')

            if self.logNum > 0:
                print '{} log re-connect#{}'.format(self.logtype, self.logNum)

            if self.logtype.startswith('kernel'):
                self.proc = subprocess.Popen(self.cmd.split(), stdout=subprocess.PIPE, universal_newlines=True)
                for line in iter(self.proc.stdout.readline, ''):
                    if not KERNEL_LOG_SPLIT:
                        found = re.match(r"\[\s*(\d+\.\d+)\].*",line)
                        if found:
                            new_time = float(found.group(1))
                            if new_time >= old_time:
                                print '[Debug] old: {}; new: {}'.format(old_time, new_time)
                                log.write(line)
                                old_time = new_time
                        else:
                            # if the beginning is not timestamp, still write into log
                            log.write(line)
                    else:
                        # write everything for seperated kernel log file 
                        log.write(line)


                print '[Debug] kernel disconnect\n'
                log.write('\n[Debug] kernel log disconnect\n')
                time.sleep(KERNEL_LOG_CHECK_GAP)

            else:
                self.proc = subprocess.Popen(self.cmd.split(),stdout=log, stderr=subprocess.STDOUT)
                (_, error) = self.proc.communicate()
                #print '[Debug] error {}'.format(error)
                print '{} log stop and log file close\n'.format(self.logtype)

            log.close()
            self.logWriteDone.set()

            # Wait ADB re-connect
            while not self.stopLog.isSet():
                #print '[Debug] {} log wait'.format(self.logtype)
                time.sleep(CHECK_ADB_GAP_TIME)
                #out = subprocess.call(['adb', '-s', self.device, LogcatCMD['get-state']])
                # below would not print error meessage on console
                out = subprocess.call(['adb', '-s', self.device, LogcatCMD['get-state']], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                if out == 0:
                    break

            if not self.stopLog.isSet():
                if not self.logtype.startswith('kernel') or KERNEL_LOG_SPLIT:
                    self.logNum = self.logNum + 1

    def stop(self):
        self.stopLog.set()
        try:
            if self.proc.poll() == None:
                print '[Debug] {} log porcess alive\n'.format(self.logtype)
                self.proc.terminate()
        except:
            print'[Debug] no {} proc runs'.format(self.logtype)
        while not self.logWriteDone.isSet():
            print '[Debug] wait log {} write'.format(self.logtype)
            time.sleep(CHECK_ADB_GAP_TIME)

        print '{} log porcess stop\n'.format(self.logtype)
        self.join()


class AdbLogcatMonitor(Thread):
    def __init__(self, device, loglist):
        Thread.__init__(self)
        self.stopLog = Event()
        self.device = device
        ts = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d_%H%M%S')
        self.logPath = path.join(PATH_BASE, ts)
        if not path.exists(self.logPath):
            makedirs(self.logPath)
            print 'log folder: \"{}\"'.format(self.logPath)

        self.logReader = []
        for logtype in loglist:
            self.logReader.append(AdbCommnd(device, self.logPath, logtype))

    def run(self):
        for reader in self.logReader:
            reader.start()

        while not self.stopLog.isSet():
            time.sleep(CHECK_ADB_GAP_TIME)

    def stop(self):
        print "Stop monitor"
        self.stopLog.set()
        for reader in self.logReader:
            reader.stop()

        self.join()

def CheckDevice():
    p = subprocess.Popen(["adb", "wait-for-device"], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    p.communicate()

    p = subprocess.Popen(["adb", "devices"], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    (out, error) = p.communicate()
    out = out.strip().split('\n')[1:]
    idx = 0
    devices = {}
    #only return 'device", not include 'host'
    # Todo: need check offline/unauthorized/no_device state

    # In below case, no logcat could be ouput. For this case, notfiy user to plugin USB to recover
    # $ adb devices
    # List of devices attached
    # HT78P1E00081    device
    # HT78P1E00081    offline

    for line in out:
        i = line.split()
        if i[1] == "device":
            devices[idx] = i[0]
            idx = idx + 1
        elif i[1] == "offline":
            print 'device {} in offline mode'.format(i[0])
            return {}
    return devices


def main():
    devices = CheckDevice()
    if len(devices) == 0:
        print "No device is connected"
        return
    elif len(devices) == 1:
        device = devices[0]
        print "device: {}".format(device)
    elif len(devices) > 1:
        print "More than 1 device connected"
        print devices
        return

    adbLM = AdbLogcatMonitor(device, LOG_LIST)
    print 'Start logging monitor...'
    adbLM.start()
    raw_input('Please press ENTER to stop logging\n\n')
    adbLM.stop()

if __name__ == '__main__':
    main()
